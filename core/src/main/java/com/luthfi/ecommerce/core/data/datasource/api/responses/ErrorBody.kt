package com.luthfi.ecommerce.core.data.datasource.api.responses

data class ErrorBody(
    val code: Int,
    val message: String
)
