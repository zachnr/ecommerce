package com.luthfi.ecommerce.core.data.datasource.api.body

data class RefreshBody(
    val token: String? = null
)
