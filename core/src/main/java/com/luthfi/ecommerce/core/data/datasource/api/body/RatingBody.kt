package com.luthfi.ecommerce.core.data.datasource.api.body

data class RatingBody(
    var invoiceId: String?,
    var rating: Int?,
    var review: String?
)
