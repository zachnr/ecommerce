package com.luthfi.ecommerce.core.data.datasource.api.responses

data class SearchResponse(
    val code: Int,
    val data: List<String>,
    val message: String
)
