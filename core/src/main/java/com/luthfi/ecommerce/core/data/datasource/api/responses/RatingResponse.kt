package com.luthfi.ecommerce.core.data.datasource.api.responses

data class RatingResponse(
    val code: Int?,
    val message: String?
)
