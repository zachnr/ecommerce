package com.luthfi.ecommerce.ui.main.store

import androidx.lifecycle.ViewModel

class SearchBottomSheetViewModel : ViewModel() {
    var category: String? = null
    var lowest: String? = null
    var highest: String? = null
    var sort: String? = null
}
